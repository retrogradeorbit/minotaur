==================
Minotaur
==================

Web based mining control and monitoring software.

Quickstart
----------

1. Clone the project
2. Run `make` in the root folder
3. When the server starts, point your browser at http://localhost:8000/admin/
4. Login with username: admin password: admin
