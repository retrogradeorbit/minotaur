from miner.models import Miner, GPU
from django.conf import settings
import time
import os

from utils.rrd import rrd

def run():

    r = rrd('test.rrd', GPU.objects.all())
    directory = settings.STATICFILES_DIRS[0]

    if not os.path.exists(directory):
        os.makedirs(directory)

    count=0
    while True:
        for hours in [days*24 for days in (365, 30, 7, 5, 3, 1)] + [12,6,3,1]:
            if count % hours == 0:
                print "writing",hours
                for key in ('temp', 'i','fan','mhsa'):
                    filename = os.path.join(directory, '{0}-{1}.png'.format(key, hours))
                    r.graph(GPU.objects.all(), name=key, filename=filename, hours=hours)

        time.sleep(5)

        count += 1
        
