from pyrrd.rrd import RRD, DS, RRA
from pyrrd.graph import ColorAttributes, Graph
from pyrrd.graph import DEF, CDEF, VDEF
from pyrrd.graph import LINE, AREA, GPRINT

from utils.cgminer import DEVICE_KEYS

import os
import time

class rrd(object):
    # the colour palette for lines on the graph
    LINE_COLOURS = (
        '#FFA902',
        '#A902FF',
        '#02A9FF',
        '#A32001',
        '#2001A3',
        '#01A320',
        '#02FFA9' )

    # the theme colours
    CA_COLOURS = {       
        "back": '#FFFFFF',
        "canvas": '#FFFFFF',
        "shadea": '#FFFFFF',
        "shadeb": '#FFFFFF',
        "mgrid": '#666666',
        "grid": '#BBBBBB',
        "axis": '#000000',
        "frame": '#FFFFFF',
        "font": '#000000',
        "arrow": '#000000'
        }

    # the seconds between data points
    STEP_TIME = 5

    def __init__(self, filename, gpus):
        self.filename = filename

        # create the minimal set of miners for these gpus
        self.miners = set()
        for gpu in gpus:
            self.miners.add(gpu.miner)

        if not os.path.exists(self.filename):
            # rrd file does not exist. create.
            ds = [
                DS(dsName='gpu{dev}_{name}'.format(dev=dev.id, name=key),
                   dsType='GAUGE',
                   heartbeat=300)
                for (key,name,unit) in DEVICE_KEYS
                for dev in gpus ]

            rows = 60*60*24/self.STEP_TIME

            rra = [
                RRA(cf='AVERAGE', xff=0, steps=1, rows=rows),    # days worth
                RRA(cf='AVERAGE', xff=0.5, steps=12, rows=rows), # 12 days worth
                RRA(cf='AVERAGE', xff=0.5, steps=12*7, rows=rows), # 12 weeks worth
                RRA(cf='AVERAGE', xff=0.5, steps=365, rows=rows), # 1 year worth
                RRA(cf='AVERAGE', xff=0.5, steps=365*5, rows=rows), # 5 years worth
            ]

            self.rrd = RRD(filename, 
                      ds=ds, 
                      rra=rra, 
                      start=int(time.time() - 2*self.STEP_TIME),
                      step=self.STEP_TIME)    # should probably start on an even boundary?
            self.rrd.create()
        else:
            # load the existing rrd file
            self.rrd = RRD(self.filename)

    def poll(self):
        print "poll"
        t = int(time.time())
        
        devs=[]
        for miner in self.miners:
            # get device summary for miners
            summary = miner.devs()
            if summary:
                devs.extend(miner.devs())
            else:
                # device summary failed, set every parameter to null
                for gpu in miner.gpu_set.all():
                    devs.append({name:'nan' for (key,name,units) in DEVICE_KEYS})

        if devs:
            # store the values for timepoint t
            vals = [dev[name] for (key,name,units) in DEVICE_KEYS
                             for dev in devs]
            self.rrd.bufferValue(t, *vals)
            self.rrd.update()

    def graph(self, gpus, name='temp', filename="graph.png", title=None, hours=12):
        # Let's set up the objects that will be added to the graph
        defs = [ DEF(rrdfile=self.filename, vname='gpu{0.id}'.format(gpu), dsName='gpu{0.id}_{name}'.format(gpu, name=name))
                 for gpu in gpus ]

        colours = self.LINE_COLOURS[:len(gpus)]

        lines = [ LINE(defObj=d, color=c, legend=str(g), width=2)
            for (d, g, c) in reversed(zip(defs, gpus, colours)) ]

        # Let's configure some custom colors for the graph
        ca = ColorAttributes()
        for key in self.CA_COLOURS:
            setattr(ca, key, self.CA_COLOURS[key])

        # Now that we've got everything set up, let's make a graph
        now = int(time.time())
        g = Graph(filename, end=now, vertical_label='"' + ({0:units for (key,n,units) in DEVICE_KEYS if key==name}[0]) + '"', 
            color=ca, start=now-60*60*hours)

        g.data.extend(defs)
        g.data.extend(lines)

        g.title = '"' + (title or dict([(a,b) for (a,b,c) in DEVICE_KEYS])[name]) + '"'
        g.width = 1200
        g.height = 200
        g.write()
        
