# decorator collection
import socket

def catch_socket_error(f):
    def new_func(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except socket.error:
            return None
    return new_func
