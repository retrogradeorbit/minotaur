"""Functions to interface with pool web software based on MPOS
https://github.com/TheSerapher/php-mpos
"""

import requests
from lxml import etree

class mpos(object):
    def __init__(self, url, username, password):
        self.url = url
        self.username = username
        self.password = password

    def _get(self, *args, **kwargs):
        """takes same parameters as requests.get
        returns a HTML node tree
        """
        req = requests.get(*args, **kwargs)
        assert req.status_code==200

        return etree.HTML(req.content)

