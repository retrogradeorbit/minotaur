MHZ = 'Megaherts'

DEVICE_KEYS = [
    ("dacc","Difficulty Accepted","Units"),
    ("temp","Temperature","Degrees C"),
    ("dref","Difficulty Rejected", "Units"),
    ("gpuv","GPU Voltage", "Volts"),
    ("gput","GPU Clock", "Megahertz"),
    ("fan","Fan Speed", "Revolutions per Minute"),
    ("gpua","GPU Activity","Units"),
    #("stat","Status","Code"),
    ("rejp","Device Rejected%", "Percent"),
    ("fanp","Fan Percent", "Percent"),
    ("rej","Rejected", "Shares"),
    ("memc","Memory Clock", "Megahertz"),
    ("mhs5","MHS 5s","Megahashes per Second"),
    ("hw","Hardware Errors","Errors"),
    ("acc","Accepted","Shares"),
    ("lsp","Last Share Pool", "Units"),
    ("work","Diff1 Work", "Work"),
    ("mh","Total MH", "Megahashes"),
    #("enab","Enabled"),
    ("hwp","Device Hardware%", "Percent"),
    ("lvw","Last Valid Work", "Units"),
    ("lst","Last Share Time", "Units"),
    ("gpu","GPU", "GPU Id"),
    ("mhsa","MHS av", "Megahashes per Second"),
    ("elap","Device Elapsed","Units"),
    ("lsd","Last Share Difficulty","Units"),
    ("i","Intensity","Units"),
    ("pt","Powertune","Units"),
    ("u","Utility","Units")
]

