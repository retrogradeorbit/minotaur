from django.db import models
from utils.decorators import catch_socket_error

import socket
import time
import os

class Rig(models.Model):
    """A single mining rig, a host on the network"""
    display_name = models.CharField(max_length=256, blank=False, null=False)
    hostname = models.CharField(max_length=256, blank=False, null=False)

    def __unicode__(self):
        return self.display_name

CLIENT_CHOICES = (
    ('cgminer', 'CGMiner - API direct connection'),
)

class Miner(models.Model):
    """A mining programme running on a rig somewhere"""
    rig = models.ForeignKey(Rig)

    # onlt cgminer atm
    client = models.CharField(max_length=8, choices=CLIENT_CHOICES, default='cgminer')

    # the port the API is on. if null, uses default port for miner
    port = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return u'Miner {0.client} on {0.rig}'.format(self) + (
            str(self.port) if self.port else '')

    def _api_cgminer(self):
        from pycgminer import CgminerAPI
        return CgminerAPI(self.rig.hostname, self.port or 4028)

    def _api_dispatch(self,*args,**kwargs):
        return {
            'cgminer':self._api_cgminer,
        }[self.client](*args, **kwargs)

    @catch_socket_error
    def description(self):
        return self._api_dispatch().status()['STATUS'][0]['Description']

    @catch_socket_error
    def HW(self):
        return self._api_dispatch().summary()['SUMMARY'][0]['Hardware Errors']

    @catch_socket_error
    def summary(self):
        return self._api_dispatch().summary()['SUMMARY'][0]

    @catch_socket_error
    def devs(self):
        try:
            return self._api_dispatch().devs()['DEVS']
        except ValueError, ve:
            return None

    @catch_socket_error
    def temps(self):
        return ", ".join(
            ["{0} C".format(d['Temperature'])
             for d in self._api_dispatch().devs()['DEVS']])

    @catch_socket_error
    def MHSav(self):
        return '{0} Mh/s'.format(self._api_dispatch().summary()['SUMMARY'][0]['MHS av'])

class GPU(models.Model):
    """A single GPU running under a miner on a rig"""
    gpu_id = models.IntegerField(default=0)

    miner = models.ForeignKey(Miner)

    def __unicode__(self):
        return u'GPU_{0.gpu_id} on {0.miner}'.format(self)

    @catch_socket_error
    def temp(self):
        return self.miner._api_dispatch().devs()['DEVS'][self.gpu_id]['Temperature']

    @catch_socket_error
    def intensity(self):
            return self.miner._api_dispatch().devs()['DEVS'][self.gpu_id]['Intensity']

