from django.contrib import admin

# Register your models here.
from models import Miner, Rig, GPU

class RigAdmin(admin.ModelAdmin):
    list_display = ('display_name',)

class MinerAdmin(admin.ModelAdmin):
    list_display = ('rig', 'client', 'port', 'description', 'HW', 'MHSav', 'temps', 'devs')

class GPUAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'gpu_id', 'miner', 'temp', 'intensity')

admin.site.register(Rig, RigAdmin)
admin.site.register(Miner, MinerAdmin)
admin.site.register(GPU, GPUAdmin)
