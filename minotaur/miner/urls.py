from django.conf.urls import patterns, url

from miner import views

urlpatterns = patterns('',
    url(r'^$', views.graph, name='graph'),
)
