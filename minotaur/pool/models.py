from django.db import models

# supported coin types
COIN_TYPES = (
    ('BTC', 'Bitcoin'),
    ('LTC', 'Litecoin'),
    ('FTC', 'Feathercoin'),
    ('DOG', 'Dogecoin')
)

# what software stack the pool front end is using
POOL_SOFTWARE = (
    ('MPOS', 'MPOS by TheSerapher'),
)

class Pool(models.Model):
    """represents a mining pool we connect to to mine, and that we need to collect coins from"""
    # the name we give this pool
    name = models.CharField(null=False, blank=False)
    
    # the base URL of the server
    url_base = models.URLField(null=False)
    
    # the type of coin mined there
    coin_type = models.CharField(null=False, max_length=3, choices=COIN_TYPES)

    # the username and password to log in to the pool
    username = models.CharField(null=False, max_length=64)
    password = models.CharField(null=False, max_length=64)

    # if you want auto collection you'll need a pin
    pin = models.CharField(null=True, blank=True, max_length=4)

    # type of software pool front end uses
    pool_software = models.CharField(null=False, blank=False, max_length=4, choices=POOL_SOFTWARE)

    def __unicode__(self):
        return u"{0.name} {0.coin_type} Pool".format(self)
