
runserver: vp
	cd minotaur && make runserver

runserver_plus: vp
	cd minotaur && make runserver_plus

vp: requirements.txt
	virtualenv vp
	vp/bin/pip install -r requirements.txt

